# Дорожная карта KDE на 2022

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)

Новому году — новая дорожная карта! Как всегда, это не какой-то официальный план или обещания. Это просто моё видение того, что сейчас находится в работе или будет развиваться в скором будущем и может быть доделано до конца года!

## Объединение настроек форматов и языков

Страницы настроек форматов и языков в Параметрах системы долгое время были проблематичными, так как их зоны ответственности пересекались, но осталось совсем чуть-чуть! Han Young [работает над объединением их в одну страницу](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1147), которая займётся обоими вопросами, делая понятнее, что за что отвечает, и предотвратит возможность запутать систему, выбрав несовместимые настройки. Я ожидаю, что это будет сделано в первой половине 2022 года.

## Капремонт значков Breeze

Дизайнер KDE Ken Vermette работает над улучшением и обновлением набора значков Breeze. Цветные значки будут немного скруглены, смягчены и графически обновлены, чтобы убрать старые некрасивые элементы, такие как длинные тени. Монохромным значкам тоже уделят внимание. Ожидается, что все они станут более отзывчивыми к выбранной цветовой схеме системы и будут лучше вписываться в интерфейс. [Первоначальная работа над значками расположений](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/189) уже проделана и отправлена на рецензию. Эти наработки будут вливаться по частям, а вы можете следить за прогрессом [в блоге](https://kver.ca/2021/12/better-adaptive-icons-for-2022/) [дизайнера](https://kver.ca/2021/11/new-icons-iconoclast-pipeline/).

## Несколько мониторов наконец должны заработать как надо

Мы планируем пустить много сил на исправление проблем, связанных с подключением нескольких мониторов. Это уже даёт свои плоды, но мы будем работать с двойным усердием в 2022!

## Инерционная прокрутка в приложениях на основе QtQuick

Недавно было внедрено [большое и важное улучшение](https://invent.kde.org/frameworks/kirigami/-/merge_requests/415), которое позволит этому случиться очень скоро — вполне вероятно, в 2022.

## Сеанс Wayland сможет полностью заменить X11

Придётся немало попотеть, но мне кажется, что это посильно. Список на странице [препятствий для перехода на Wayland](https://community.kde.org/Plasma/Wayland_Showstoppers) довольно короткий, и когда туда добавляются новые пункты, они обычно куда более низкой важности, чем те, что уже исправлены. Теперь же, когда NVIDIA добавила поддержку механизма GBM в свой драйвер, а диспетчер окон KWin уже его поддерживает, я думаю, что мы сможем угодить пользователям NVIDIA, которые представляют собой большу́ю часть недовольных пользователей Plasma и до сих пор едва могут использовать Wayland вообще. Пусть этот пункт будет здесь с большой натяжкой, но я считаю, что шанс есть!

## Инициатива «15-минутные ошибки»

В этом году я хочу запустить то, что сам называю «15-минутные ошибки»: попытку исправить как можно больше проблем-выскочек, дающих о себе знать в течение четверти часа обычного пользования компьютером. Это те самые ошибки, которые формируют у людей негативное мнение и подкрепляют слухи о том, что программное обеспечение KDE проблематичное и ненадёжное.

Поначалу я думаю ограничиться Plasma и всем, что с ней связано (KWin, Параметры системы, центр приложений Discover), чтобы не захлебнуться масштабом. Но если это станет популярным и успешным проектом, то я хотел бы расширить рамки также на приложения и библиотеки! Взгляните на текущий список задач [здесь](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=1954983&order=changeddate%20DESC%2Cbug_severity%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced). Я опубликую больше подробностей в скором будущем!

Вот такой получился список! А вы что думаете? Как считаете, на чём ещё нам стоит сосредоточиться в 2022?

_Перевод:_ [Иван Ткаченко (ratijas)](https://t.me/ratijas)  
_Источник:_ https://pointieststick.com/2022/01/03/kde-roadmap-for-2022/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
