# На этой неделе в KDE: Getting Plasma 5.24 ready for release

> 23–30 января, основное — прим. переводчика

Plasma 5.24 is almost ready!

I mentioned last week that I haven’t been posting about fixes for regressions in 5.24 that never got released, because there would be too many. Nonetheless, people have been working very hard on this, and we’re [down to only 7](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=1968876&query_format=advanced&version=5.23.90), with two of them having open merge requests! Working on those is appreciated, as it helps improve the stability of the final release in a week and a half.

## Исправленные «15-минутные ошибки»

Current number of bugs: **83, down from 87**. [Current list of bugs](https://tinyurl.com/plasma-15-minute-bugs)

After waking up the system, [the desktop is no longer sometimes shown for a moment before the lock screen appears](https://bugs.kde.org/show_bug.cgi?id=316734) (David Edmundson, Plasma 5.25)

## Новые возможности

Konsole [now lets you automatically switch to a different profile when you connect to a specific remote server using SSH](https://invent.kde.org/utilities/konsole/-/merge_requests/219) (Tomaz Canabrava, Konsole 22.04):

<https://i.imgur.com/1NbAIaA.mp4?_=1>

Dolphin [now optionally lets you see images’ dimensions below their icons in icon view](https://invent.kde.org/system/dolphin/-/merge_requests/332) (Kai Uwe Broulik, Dolphin 22.04):

![1](https://pointieststick.files.wordpress.com/2022/01/image-dimensions.png)

## Wayland

In the Plasma Wayland session, KWin [no longer sometimes crashes when dragging screenshots from Spectacle to XWayland apps](https://bugs.kde.org/show_bug.cgi?id=448920) (David Edmundson, Plasma 5.24)

In the Wayland session, when an app is started in fullscreen mode and then made windowed, [it will now be placed in a location that respects the current window placement mode, rather than always appearing in the top-left corner of the screen](https://bugs.kde.org/show_bug.cgi?id=448856) (Влад Загородний, Plasma 5.24)

In the Wayland session, [screencasting no longer causes the cursor to be visually clipped](https://bugs.kde.org/show_bug.cgi?id=448840) (Влад Загородний, Plasma 5.24)

In the Wayland session, when you unplug and re-plug an external screen, XWayland apps that want to launch on the primary screen (such as many games) [no longer to get confused and open on the wrong screen](https://bugs.kde.org/show_bug.cgi?id=449099) (Aleix Pol Gonzalez, Plasma 5.24)

In the Wayland session, cursor app launch feedback effects [now respect the global timeout value for it that you can set in the System Settings Launch Feedback page](https://bugs.kde.org/show_bug.cgi?id=438622) (David Redondo, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Gwenview [now launches a bit faster, particularly when there are a lot of remote mounts](https://invent.kde.org/graphics/gwenview/-/merge_requests/133) (Nicolas Fella, Gwenview 22.04)

Plasma [no longer sometimes crashes on login when certain apps that display System Tray items launch automatically](https://bugs.kde.org/show_bug.cgi?id=442463) (Konrad Materka, Plasma 5.24)

Keyboard navigation between widgets in the Widget Explorer sidebar [now works that way you would expect it to](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/820) (Noah Davis, Plasma 5.24)

Disk read/write sensors in System Monitor widgets and the app of the same name [no longer report bogus values the first time they update](https://bugs.kde.org/show_bug.cgi?id=448494) (Arjen Hiemstra, Plasma 5.24)

System Monitor widgets [can now be dragged using touch while in Edit Mode](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1409) (Marco Martin, Plasma 5.24)

Custom icons that use SVG images referred to by their path rather than their name once again appear correctly [on folders](https://bugs.kde.org/show_bug.cgi?id=448596) and [apps on the desktop](https://bugs.kde.org/show_bug.cgi?id=448116) (Fushan Wen, Frameworks 5.91)

KDE apps like Dolphin that scan for network mounts and disks when launched [now launch more quickly when you have a lot of Snap apps installed or ISO images mounted](https://invent.kde.org/frameworks/solid/-/merge_requests/73) (Kai Uwe Broulik, Frameworks 5.91)

This isn’t KDE software, but it affected a lot of our users, so I’m listing it here anyway: Firefox [no longer constantly asks to be made the default browser on launch when it’s being run with the `GTK_USE_PORTAL=1` environment variable (as some distros do by default) to make it use KDE file dialogs instead of GNOME file dialogs](https://bugzilla.mozilla.org/show_bug.cgi?id=1516290) (Emilio Cobos Álvarez and Robert Mader, Firefox 98)

## Улучшения пользовательского интерфейса

Dolphin’s shortcuts configuration window [now includes shortcuts from Konsole that will be used in the embedded terminal view, so you can re-assign them if you’d like](https://bugs.kde.org/show_bug.cgi?id=428265) (Stephan Sahm, Dolphin 22.04)

KRunner [now returns better search results for very short search strings of only one or two letters](https://bugs.kde.org/show_bug.cgi?id=443866) (Alexander Lohnau, Plasma 5.24)

You [can now find KMenuEdit in app launchers, KRunner, and Discover](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/5) (Nate Graham, Plasma 5.25):

![2](https://pointieststick.files.wordpress.com/2022/01/kmenuedit.png)

Menu items in QtWidgets-based apps [now also become taller when in Tablet Mode, just like menu items in QtQuick apps](https://invent.kde.org/plasma/breeze/-/merge_requests/172)! However Unlike those, QtWidgets apps will have to be restarted first due to technical limitations (Jan Blackquill, Plasma 5.25)

Discover [now provides an indication of the number of search results and the number of items in the currently viewed category](https://bugs.kde.org/show_bug.cgi?id=389616) (Aleix Pol Gonzalez, Plasma 5.25):

![3](https://pointieststick.files.wordpress.com/2022/01/number-of-search-results-in-discover-1.png)

Spacing between System Tray icons [is now configurable, and automatically switches to its widest setting when in Tablet Mode](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1394) (Nate Graham, Plasma 5.25)

System Settings’ Display Configuration page [now calls your device’s built-in screen a «Built-in Screen»](https://invent.kde.org/plasma/kscreen/-/merge_requests/80), rather than assuming it is a laptop and calling it a «Laptop Screen» (David Edmundson, Plasma 5.25)

## Как вы можете помочь

Если вы разработчик, посмотрите остающиеся [регрессии в Plasma 5.24](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=1968876&query_format=advanced&version=5.23.90) или нашу новую инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев](https://t.me/msmarshev)  
_Источник:_ https://pointieststick.com/2022/01/28/this-week-in-kde-getting-plasma-5-24-ready-for-release/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
