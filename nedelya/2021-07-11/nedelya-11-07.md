# На этой неделе в KDE: прощание с давними багами

> 4–11 июля, основное — прим. переводчика

На этой неделе были исправлены многие старые ошибки! В списке каждый найдёт пару досадных проблем, которые больше не будут его беспокоить! Кроме этого, удалось проделать немало серьёзной работы над Wayland. Только взгляните:

## Новые возможности

Файловый менеджер Dolphin [теперь может отображать несколько сменяющихся миниатюр](https://invent.kde.org/system/dolphin/-/merge_requests/167) при наведении курсора на файлы и папки (David Lerch, Dolphin 21.08):

<https://i.imgur.com/9csfF46.mp4?_=1>

Правила окон KWin [теперь можно применять к особым окнам OSD](https://invent.kde.org/plasma/kwin/-/merge_requests/1155), таким как окно режима «картинка в картинке» в Firefox (David Edmundson, Plasma 5.23)

## Wayland

В сеансе Plasma Wayland окна GTK-приложений [больше не разлетаются за пределы экрана при перемещении с помощью сенсорного ввода](https://bugs.kde.org/show_bug.cgi?id=431489) со включённым эффектом «Колышущиеся окна» (Xaver Hugl, Plasma 5.22.3)

При использовании видеокарты Nvidia с проприетарным драйвером окна XWayland [больше не прекращают обновляться в некоторых случаях](https://invent.kde.org/plasma/kwin/-/merge_requests/1130) после переключения между полноэкранным и оконным режимами (Erik Kurzinger, Plasma 5.23)

Сенсорный ввод [больше не прекращает работать после выполнения операций типа «Распахнуть окно»](https://bugs.kde.org/show_bug.cgi?id=430560) в сеансе Wayland (Xaver Hugl, Plasma 5.22.3)

Окна приложений GTK [больше не отображают различные элементы пользовательского интерфейса слишком маленькими](https://bugs.kde.org/show_bug.cgi?id=438971) при использовании большого коэффициента масштабирования в сеансе Wayland (David Edmundson, Plasma 5.22.4)

Контекстное меню элементов панели задач [больше не закрывается вместе с исчезновением всплывающей подсказки задачи](https://bugs.kde.org/show_bug.cgi?id=417939) в сеансе Wayland (David Redondo, Plasma 5.23)

Кнопка распахивания окна на весь экран [больше не отображается для некоторых окон с неизменяемым размером](https://bugs.kde.org/show_bug.cgi?id=439578) в сеансе Wayland (Plasma 5.23)

Нативные приложения Wayland, использующие «подповерхности» и указывающие расположить их под родительским окном, [теперь размещаются правильно](https://bugs.kde.org/show_bug.cgi?id=438808) (Влад Загородний, Plasma 5.23)

## Исправления ошибок и улучшения производительности

Plasma [больше не зависает](https://bugs.kde.org/show_bug.cgi?id=438808), когда всплывающая подсказка пытается отобразить обложку альбома, доступ к которой либо замедлен, либо невозможен в данный момент (Kai Uwe Broulik, Plasma 5.22.3)

Пользователи [больше не получают уведомления о признаках нестабильности дисков со включённым S.M.A.R.T.](https://invent.kde.org/plasma/plasma-disks/-/merge_requests/21) Это приводило к частым оповещениям из-за ложных срабатываний в случаях, когда диски неправильно сообщали о своём статусе или характеризовали переходные состояния как нестабильность. Уведомления об обычных ошибках продолжат приходить! (Harald Sitter, Plasma 5.22.4)

Всплывающие подсказки панели задач [теперь потребляют меньше памяти](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/524) (Fushan Wen, Plasma 5.23)

[Ускорен доступ к глобальным файлам конфигурации](https://invent.kde.org/frameworks/kconfig/-/merge_requests/70), что должно сделать чуть быстрее многие операции, включая запуск приложений (Aleix Pol Gonzalez, Frameworks 5.85)

## Улучшения пользовательского интерфейса

[Несколько](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/22) [диалоговых](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/23) [окон](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/21) [в приложениях](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/24) из набора Kontact, в частности KMail, были обновлены и избавлены от лишних рамок (Carl Schwan, KDE PIM 21.08):

* ![1](https://pointieststick.files.wordpress.com/2021/07/singlefileinstance.png)

* ![2](https://pointieststick.files.wordpress.com/2021/07/ical.png)

* ![3](https://pointieststick.files.wordpress.com/2021/07/openexchange.png)

* ![4](https://pointieststick.files.wordpress.com/2021/07/google_groupware.png)

Теперь во время воспроизведения медиа миниатюры окон в панели задач [показывают обложку альбома, только если заголовок окна совпадает с названием проигрываемого медиа](https://bugs.kde.org/show_bug.cgi?id=409980). Это должно предотвратить показ для окон обложек вместо миниатюр почти всегда (Bharadwaj Raju, Plasma 5.23):

![5](https://pointieststick.files.wordpress.com/2021/07/screenshot_20210708_214433.png)

*И да, над тем, что название медиа и элементы управления воспроизведением дублируются, уже ведётся работа!*

Список вариантов частоты обновления экрана на странице настройки экранов в Параметрах системы [теперь отсортирован по убыванию](https://bugs.kde.org/show_bug.cgi?id=439515) (Иван Ткаченко, Plasma 5.23)

Диалоговое окно выбора значков [получило основательное обновление интерфейса](https://bugs.kde.org/show_bug.cgi?id=388807) и [теперь корректно отображает SVG на экранах с высоким разрешением](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/33#note_267345) (Kai Uwe Broulik, Frameworks 5.85):

![6](https://pointieststick.files.wordpress.com/2021/07/screenshot_20210708_213959.png)

Страницы «О программе» в приложениях на основе Kirigami теперь [упоминают полное имя приложения](https://invent.kde.org/frameworks/kirigami/-/merge_requests/331) и [отображают роль/тип сделанной работы для каждого участника](https://bugs.kde.org/show_bug.cgi?id=438442) (Felipe Kinoshita, Frameworks 5.85):

![7](https://pointieststick.files.wordpress.com/2021/07/screenshot_20210708_214222.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [boingo-00](https://t.me/boingo00)  
_Источник:_ https://pointieststick.com/2021/07/08/this-week-in-kde-fixing-longstanding-bugs/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
