# На этой неделе в KDE: Бета-версия Plasma 5.24

На этой неделе мы выпустили бета-версию Plasma 5.24, [так что просим скачать её](https://vk.com/kde_ru?w=wall-33239_10983) и [писать отчёты об ошибках](https://bugs.kde.org/)! Мы потратили бо́льшую часть недели, готовясь к этому и исправляя ошибки, что мы и продолжим делать в следующем месяце для подготовки итогового выпуска.

> 9–16 января, основное — прим. переводчика

## Новые возможности

Виджет «Диски и устройства» [теперь предлагает возможность открыть Диспетчер разделов на указанном разделе](https://bugs.kde.org/show_bug.cgi?id=446897) (Nate Graham, Диспетчер разделов 22.04):

![0](https://pointieststick.files.wordpress.com/2022/01/big-goal-repartitioning.png)

Теперь можно настроить приложения для открытия ссылок вида [geo://](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/756) и [tel://](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/787) (Volker Krause и Kai Uwe Broulik, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2022/01/more-default-apps.png)

[Теперь можно щёлкнуть колёсиком мыши по виджету Bluetooth, чтобы включить или выключить Bluetooth](https://bugs.kde.org/show_bug.cgi?id=427816) (Nate Graham, Plasma 5.25)

Разделы Параметров системы и Информации о системе [теперь можно найти по их ключевым словам при помощи строки поиска, меню приложений, эффекта «Обзор» и т.д.](https://bugs.kde.org/show_bug.cgi?id=445304) (Alexander Lohnau, Plasma 5.24)

## Wayland

[Раздел управления шрифтами в Параметрах системы теперь доступен](https://bugs.kde.org/show_bug.cgi?id=439375) в сеансе Wayland (David Edmundson, Plasma 5.24)

Центр справки [больше не должен аварийно закрываться случайным образом при движении курсора или наведении на ссылки](https://bugs.kde.org/show_bug.cgi?id=422972) (Christoph Cullmann, Frameworks 5.91)

Открытие и закрытие боковой панели виджетов [больше не приводит к перемещению окон](https://bugs.kde.org/show_bug.cgi?id=426969) (David Edmundson, Frameworks 5.91)

## Исправления ошибок и улучшения производительности

Диалог перезаписи, который появляется при извлечении архиватором Ark файлов, имеющих то же имя, что у других файлов в месте назначения, [больше не вводит в заблуждение фразой «Файлы идентичны»](https://bugs.kde.org/show_bug.cgi?id=436556) (Albert Astals Cid, Ark 22.04)

Создание снимка экрана при помощи Spectacle с использованием флагов в командной строке (например, `spectacle -bc`) [больше не приводит к отображению двух уведомлений](https://bugs.kde.org/show_bug.cgi?id=447517) (Antonio Prcela, Spectacle 22.04)

Раздел управления принтерами в Параметрах системы [теперь нормально отображает длинные имена принтеров, если используется масштабирование экрана](https://bugs.kde.org/show_bug.cgi?id=419916) (Kai Uwe Broulik, print-manager 22.04)

Отключение монитора [больше не приводит к исчезновению панелей в некоторых случаях](https://bugs.kde.org/show_bug.cgi?id=447936) (Marco Martin, Plasma 5.24)

Установка дополнений с зависимостями [снова работает](https://bugs.kde.org/show_bug.cgi?id=448237) (Alexander Lohnau, Frameworks 5.91, однако дистрибутивам следует портировать это исправление в 5.90 как можно скорее)

Разделы Параметров системы с кнопками для установки дополнений [теперь используют меньше памяти](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/161) (Alexander Lohnau, Frameworks 5.91)

## Улучшения пользовательского интерфейса

Область просмотра папок на рабочем столе [теперь всегда отображает подсказки для значков, чьи подписи сокращены, как в диспетчере файлов Dolphin](https://bugs.kde.org/show_bug.cgi?id=433386) (Nate Graham, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2022/01/folder-view-tooltip.png)

Поля поиска в приложениях на основе Kirigami [теперь содержат маленький значок лупы, и у него даже есть эффект исчезновения при фокусировке на поле поиска](https://invent.kde.org/frameworks/kirigami/-/merge_requests/465) (Carl Schwan, Frameworks 5.91):

<https://i.imgur.com/Oebe0Dz.mp4?_=1>

Боковая панель Dolphin [теперь содержит значок извлечения рядом с устройствами, которые можно извлечь](https://bugs.kde.org/show_bug.cgi?id=154499) (Kai Uwe Broulik, Frameworks 5.91):

![4](https://pointieststick.files.wordpress.com/2022/01/eject-symbol.png)

Меню на основе библиотеки KHamburgerMenu [теперь обладают более простым дизайном для пунктов в конце списка](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/88): теперь в самом конце присутствует пункт «Дополнительно», который показывает оставшиеся пункты меню, а прямо над ним расположен пункт «Справка», причём у обоих пунктов стоят правильные значки (Mufeed Ali, Frameworks 5.91):

![5](https://pointieststick.files.wordpress.com/2022/01/new-khamburgermenu-design.png)

Нижние панели навигации [теперь используют новый стиль выделения](https://invent.kde.org/frameworks/kirigami/-/merge_requests/466) (Felipe Kinoshita, Frameworks 5.91):

![6](https://pointieststick.files.wordpress.com/2022/01/prettier-bottom-nav.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ Пётр Шкенев  
_Источник:_ https://pointieststick.com/2022/01/14/this-week-in-kde-the-plasma-5-24-beta/  

<!-- 💡: Help Center → Центр справки -->
<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
