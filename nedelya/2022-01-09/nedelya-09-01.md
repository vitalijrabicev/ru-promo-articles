# На этой неделе в KDE: улучшенная поддержка MTP

> 2–9 января, основное — прим. переводчика

## Новые возможности

Всплывающие подсказки панели задач для окон, воспроизводящих аудио, теперь [показывают](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/788) ползунок громкости под кнопками управления воспроизведением (Noah Davis, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2022/01/with-volume-control.png)

## Wayland

В сеансе Plasma Wayland [исправлена](https://bugs.kde.org/show_bug.cgi?id=446061) ситуация, когда миниатюры окон могли не отображаться во всплывающих подсказках панели задач при определённых конфигурациях (David Edmundson, Plasma 5.24)

В сеансе Plasma Wayland элемент системного лотка для показа и скрытия виртуальной клавиатуры теперь [становится](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1335) активным только в режиме планшета (Nate Graham, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Okular теперь надёжнее [открывает](https://invent.kde.org/graphics/okular/-/merge_requests/532) [и](https://invent.kde.org/graphics/okular/-/merge_requests/535) [подписывает](https://invent.kde.org/graphics/okular/-/merge_requests/528) различные защищённые паролем документы (Albert Astals Cid, Okular 21.12.1)

Связь с устройствами MTP теперь в целом работает намного лучше: теперь они правильно [отображаются](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1330) в виджете «Диски и устройства»; при открытии их в Dolphin область просмотра теперь [обновляется](ttps://bugs.kde.org/show_bug.cgi?id=440794) автоматически, когда вы следуете предоставленной инструкции, разблокируете устройство и предоставляете доступ; а инструкции [стали](https://invent.kde.org/network/kio-extras/-/merge_requests/140) понятнее и полезнее (Harald Sitter и James John, Plasma 5.24 и Dolphin 22.04)

Устройства Bluetooth, которые подключаются нестандартным способом, например, беспроводные контроллеры PlayStation Dualshock 3, теперь [появляются](https://bugs.kde.org/show_bug.cgi?id=432715) в виджете Bluetooth после подключения (Bart Ribbers, Plasma 5.23.5)

Выключение и последующее включение монитора больше [не приводит](https://bugs.kde.org/show_bug.cgi?id=447419) иногда к изменению размера некоторых окон (Xaver Hugl, Plasma 5.24)

Нажатие кнопки «Приостановить индексирование» на странице настройки поиска файлов в Параметрах системы теперь действительно [приостанавливает](https://bugs.kde.org/show_bug.cgi?id=443693) индексирование (Yerrey Dev, Plasma 5.24)

Теперь вы можете [изменить](https://bugs.kde.org/show_bug.cgi?id=444624) пользователя или группу файла или папки на рабочем столе (Ahmad Samir, Frameworks 5.91)

Приложения Snap больше [не отображаются](https://invent.kde.org/frameworks/solid/-/merge_requests/65) в качестве смонтированных томов в боковой панели Dolphin (Kai Uwe Broulik, Frameworks 5.91)

Переназначение клавиш на странице дополнительных параметров клавиатуры Параметров системы теперь приводит к тому, что любые заменённые клавиши-модификаторы правильно [обрабатываются](https://bugs.kde.org/show_bug.cgi?id=426684) в глобальных комбинациях клавиш (Fabian Vogt, Frameworks 5.90)

## Улучшения пользовательского интерфейса

Виджет «Батарея и яркость» теперь [превращается](https://bugs.kde.org/show_bug.cgi?id=415073) в виджет «Яркость» на компьютерах, где нет батарей, но поддерживается регулировка яркости (Aleix Pol Gonzalez, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2022/01/brightness-1.png)

Виджеты Plasma с прокручиваемыми представлениями [теперь](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/96) [используют](https://invent.kde.org/plasma/bluedevil/-/merge_requests/66) [более последовательный](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/107) [стиль](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1328) (Carl Schwan, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2022/01/scrollbar.png)

Анимация «Масштабирование» теперь [используется](https://invent.kde.org/plasma/kwin/-/merge_requests/1845) по умолчанию для открытия и закрытия окон вместо старой анимации «Растворение» (Влад Загородний, Plasma 5.24)

Файлы и папки на рабочем столе теперь [автоматически выделяются](https://bugs.kde.org/show_bug.cgi?id=434513) при создании или перемещении (Derek Christ, Plasma 5.24)

Когда вы включаете автоматический вход в учётную запись пользователя, вас теперь [предупреждают](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/20) о некоторых изменениях, которые вы, возможно, захотите внести в настройки службы KWallet (Nate Graham, Plasma 5.24):

![3](https://pointieststick.files.wordpress.com/2022/01/message.png)

*«Автоматический вход в систему не поддерживает автоматическую разблокировку вашего бумажника KDE, поэтому он будет просить вас о разблокировке каждый раз, когда вы входите в систему. Чтобы избежать этого, вы можете изменить пароль бумажника на пустой. Обратите внимание, что это небезопасно и должно выполняться только в доверенной среде».*

Значок Yakuake в системном лотке теперь [монохромный](https://bugs.kde.org/show_bug.cgi?id=427485) (Артём Гринёв и Bogdan Covaciu, Frameworks 5.91):

![5](https://pointieststick.files.wordpress.com/2022/01/yakuake-tray-icon.png)

Выпадающие меню в приложениях на основе Qt Quick теперь [имеют](https://bugs.kde.org/show_bug.cgi?id=447289) тот же размер и внешний вид, что и меню в приложениях Qt Widgets (Nate Graham, Frameworks 5.91)

Ползунками в приложениях на основе Qt Quick теперь [можно управлять](https://bugs.kde.org/show_bug.cgi?id=417211) прокруткой колёсика мыши над ними, как и в других приложениях (Nate Graham, Frameworks 5.91)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://pointieststick.com/2022/01/07/this-week-in-kde-better-mtp-support/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
